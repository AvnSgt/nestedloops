
#include <stdio.h>

int main(void)
{   
    /* Const Vaule for ROWs the row/column grid 
    matrix is 8x8, but a max column setting is 
    not needed. */
    const int ROWS_MAX = 8;
    /* character value set to @, but could be any value */
    char atSymb = '@';
    /* Outer loop to iterate over rows and print new 
    line when needed. */
    for (int row = 3; row <= ROWS_MAX; row++){
        /*Inner loop to iterate over each column 
        and print the symbol */
        for (int column = 1; column <= row; column++){
            printf("%c", atSymb);
        
        }
        printf("\n");
    }
            
    return 0;
}
